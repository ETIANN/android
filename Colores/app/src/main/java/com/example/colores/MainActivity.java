package com.example.colores;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RadioGroup grupo1, grupo2;
    RadioButton r1, r2, r3, r4, r5, r6;
    TextView texto;
    CheckBox muestraT;
    String text, sinTexto="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto = findViewById(R.id.textView);
        grupo1 = findViewById(R.id.grupo1);
        grupo2 = findViewById(R.id.grupo2);
        r1 = findViewById(R.id.radioButton);
        r2 = findViewById(R.id.radioButton2);
        r3 = findViewById(R.id.radioButton3);
        r4 = findViewById(R.id.radioButton4);
        r5 = findViewById(R.id.radioButton5);
        r6 = findViewById(R.id.radioButton6);
        muestraT = findViewById(R.id.checkBox);
        muestraT.setChecked(true);
        text = texto.getText().toString();
        grupo1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(grupo1.getCheckedRadioButtonId()==r1.getId()){
                    texto.setBackgroundColor(getResources().getColor(R.color.fondoNegro));
                }
                else if(grupo1.getCheckedRadioButtonId()==r2.getId()){
                    texto.setBackgroundColor(getResources().getColor(R.color.fondoVerde));
                }
                else if(grupo1.getCheckedRadioButtonId()==r3.getId()){
                    texto.setBackgroundColor(getResources().getColor(R.color.fondoRojo));
                }
            }
        });

        grupo2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(grupo2.getCheckedRadioButtonId()==r4.getId()){
                    texto.setTextColor(getResources().getColor(R.color.textoBlanco));
                }
                else if(grupo2.getCheckedRadioButtonId()==r5.getId()){
                    texto.setTextColor(getResources().getColor(R.color.textoAmarillo));
                }
                else if(grupo2.getCheckedRadioButtonId()==r6.getId()){
                    texto.setTextColor(getResources().getColor(R.color.textoAzul));
                }
            }
        });
      muestraT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if (muestraT.isChecked()){
                  texto.setText(text);
              }
              else{
                  texto.setText(sinTexto);
              }
          }
      });
    }



}
