package com.example.practica_03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Actividad_04 extends AppCompatActivity implements View.OnClickListener {

    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, bSuma, bResta, bMult, bDiv, bIgual, bBorrar;
    TextView texto;

    int resultado=0;
    String num="", aux="";

    int op=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_04);
        b1 = (Button) findViewById(R.id.button1);
        b2 = (Button)findViewById(R.id.button2);
        b3 = (Button)findViewById(R.id.button3);
        b4 = (Button)findViewById(R.id.button4);
        b5 = (Button)findViewById(R.id.button5);
        b6 = (Button)findViewById(R.id.button6);
        b7 = (Button)findViewById(R.id.button7);
        b8 =(Button) findViewById(R.id.button8);
        b9 = (Button)findViewById(R.id.button9);
        b0 = (Button)findViewById(R.id.button0);
        bSuma = (Button)findViewById(R.id.buttonS);
        bResta = (Button)findViewById(R.id.buttonR);
        bMult = (Button)findViewById(R.id.buttonMult);
        bDiv = (Button)findViewById(R.id.buttonDiv);
        bIgual = (Button)findViewById(R.id.buttonIg);
        bBorrar = (Button)findViewById(R.id.buttonBorrar);
        texto = findViewById(R.id.textView);


        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        b0.setOnClickListener(this);
        bSuma.setOnClickListener(this);
        bResta.setOnClickListener(this);
        bMult.setOnClickListener(this);
        bDiv.setOnClickListener(this);
        bIgual.setOnClickListener(this);
        bBorrar.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {


            switch (v.getId()) {
                case R.id.button1:
                    num += "1";
                    texto.setText(num);
                    break;

                case R.id.button2:
                    num += "2";
                    texto.setText(num);
                    break;

                case R.id.button3:
                    num += "3";
                    texto.setText(num);
                    break;

                case R.id.button4:
                    num += "4";
                    texto.setText(num);
                    break;

                case R.id.button5:
                    num += "5";
                    texto.setText(num);
                    break;

                case R.id.button6:
                    num += "6";
                    texto.setText(num);
                    break;

                case R.id.button7:
                    num += "7";
                    texto.setText(num);
                    break;

                case R.id.button8:
                    num += "8";
                    texto.setText(num);
                    break;

                case R.id.button9:
                    num += "9";
                    texto.setText(num);
                    break;

                case R.id.button0:
                    num += "0";
                    texto.setText(num);
                    break;


                case R.id.buttonS:
                    op=1;
                    texto.setText("");
                    aux=num;
                    num="";
                    break;

                case R.id.buttonR:
                    op=2;
                    texto.setText("");
                    aux=num;
                    num="";
                    break;

                case R.id.buttonMult:
                    op=3;
                    texto.setText("");
                    aux=num;
                    num="";
                    break;

                case R.id.buttonDiv:
                    op=4;
                    texto.setText("");
                    aux=num;
                    num="";
                    break;

                case R.id.buttonBorrar:
                    texto.setText("");
                    num="";
                    aux="";
                    resultado=0;
                    break;

                case R.id.buttonIg:

                    resuelveOperacion(aux, num, op);
                    // texto.setText(op);

                    break;
            }


    }

    private void resuelveOperacion(String aux, String num, int op) {
        try {

            if(op==2) {
                resultado = Integer.parseInt(aux) - Integer.parseInt(num);

                texto.setText(Integer.toString(resultado));
            }
            else if (op==1){
                resultado = Integer.parseInt(aux) + Integer.parseInt(num);
                texto.setText(Integer.toString(resultado));
            }
            else if (op==3) {
                resultado = Integer.parseInt(aux) * Integer.parseInt(num);
                texto.setText(Integer.toString(resultado));
            }
            else if (op==4) {
                resultado = Integer.parseInt(aux) / Integer.parseInt(num);
                texto.setText(Integer.toString(resultado));
            }

        }catch(NumberFormatException nfe){

        }
    }


}
