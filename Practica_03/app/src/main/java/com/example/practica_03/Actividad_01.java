package com.example.practica_03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Actividad_01 extends AppCompatActivity implements View.OnClickListener {

    Button boton;
    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_01);
        texto=findViewById(R.id.texto);
        texto.setVisibility(View.INVISIBLE);
        boton=findViewById(R.id.boton);
        boton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        boton.setVisibility(View.GONE);
        texto.setVisibility(View.VISIBLE);
    }
}
