package com.example.practica_03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Actividad_03 extends AppCompatActivity implements View.OnClickListener {

    Button ej1, ej2, ej3, ej4;
    Intent i1, i2, i3, i4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_03);

        ej1= findViewById(R.id.ej1);
        ej2= findViewById(R.id.ej2);
        ej3= findViewById(R.id.ej3);
        ej4= findViewById(R.id.ej4);
        ej1.setOnClickListener(this);
        ej2.setOnClickListener(this);
        ej3.setOnClickListener(this);
        ej4.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ej1:
                i1= new Intent(Actividad_03.this, Actividad_01.class);
                startActivity(i1);
                break;
            case R.id.ej2:
                i2= new Intent(Actividad_03.this, Actividad_02.class);
                startActivity(i2);
                break;
            case R.id.ej3:
                i3= new Intent(Actividad_03.this, Actividad_03.class);
                startActivity(i3);
                break;
            case R.id.ej4:
                i4= new Intent(Actividad_03.this, Actividad_04.class);
                startActivity(i4);
                break;
        }
    }
}
