package com.example.puzle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button barajar, b1, b2, b3, b4, b5, b6, b7, b8, b9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        barajar = findViewById(R.id.barajar);
        barajar.setOnClickListener(this);
        b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this);
        b2 = findViewById(R.id.b2);
        b2.setOnClickListener(this);
        b3 = findViewById(R.id.b3);
        b3.setOnClickListener(this);
        b4 = findViewById(R.id.b4);
        b4.setOnClickListener(this);
        b5 = findViewById(R.id.b5);
        b5.setOnClickListener(this);
        b6 = findViewById(R.id.b6);
        b6.setOnClickListener(this);
        b7 = findViewById(R.id.b7);
        b7.setOnClickListener(this);
        b8 = findViewById(R.id.b8);
        b8.setOnClickListener(this);
        b9 = findViewById(R.id.b9);
        b9.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId()==barajar.getId()) {

            Button[] arrayBotones = new Button[]{b1, b2, b3, b4, b5, b6, b7, b8, b9};
            int n=8;  //numeros aleatorios
            int k=n;  //auxiliar;
            int[] numeros=new int[n];
            int[] resultado=new int[n];
            Random rnd=new Random();
            int res;


            //se rellena una matriz ordenada del 1 al 8(1..n)
            for(int i=0;i<n;i++){
                numeros[i]=i+1;
            }

            for(int i=0;i<n;i++){
                res=rnd.nextInt(k);
                resultado[i]=numeros[res];
                numeros[res]=numeros[k-1];

                k--;

            }
            for (int i=0; i<n; i++){
                arrayBotones[i].setText(Integer.toString(numeros[i]));
            }



        }
    }


}
