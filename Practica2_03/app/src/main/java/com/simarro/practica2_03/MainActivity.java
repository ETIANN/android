package com.simarro.practica2_03;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Bundle info;
    Intent i;
    String nombre;
    EditText nom;
    Button ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nom = findViewById(R.id.nombre);
        nombre= nom.getText().toString();
        ok = findViewById(R.id.ok);
        ok.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        i = new Intent(MainActivity.this, Main2Activity.class);
        info = new Bundle();
        info.putString("info", nombre);
        i.putExtras(info);
        startActivityForResult(i,1);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode,@Nullable Bundle options ) {
        super.startActivityForResult(intent, requestCode, options);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_CANCELED){
            Log.i("TAG_PRUEBA","RESULTADO CANCELADO");

        }
        else{
            String resultado = data.getExtras().getString("RESULTADO");
            nom.setText(resultado);

        }
    }


}
