package com.simarro.practica2_03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    Button aceptar;
    Button cancelar;
    EditText texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        aceptar = findViewById(R.id.aceptar);
        cancelar = findViewById(R.id.cancelar);
        texto = findViewById(R.id.editText);

        aceptar.setOnClickListener(this);
        cancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.aceptar){
            String resultado = texto.getText().toString();
            Intent i = getIntent();
            i.putExtra("RESULTADO",resultado);
            setResult(RESULT_OK, i);
            finish();
        }
        else{
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
