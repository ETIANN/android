package com.simarro.proyectoetienne;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;


import androidx.appcompat.app.AppCompatActivity;

public class FormularioContacto  extends AppCompatActivity implements View.OnClickListener {

    EditText nombre, correo, telefono;
    Spinner sexo;
    Button crear;
    Contacto c;
    Intent i ;
    CheckBox checkAmigo;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);
        i = getIntent();
        nombre=findViewById(R.id.etf_nombre);
        sexo=findViewById(R.id.spinner);
        correo=findViewById(R.id.etf_correo);
        telefono=findViewById(R.id.etf_telf);
        checkAmigo=findViewById(R.id.checkBox);
        crear=findViewById(R.id.bf_crear);
        crear.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v.getId()==crear.getId()) {
            if (nombre.getText().toString().isEmpty() || sexo.getSelectedItem().toString().isEmpty() || correo.getText().toString().isEmpty() || telefono.getText().toString().isEmpty()){
               c=new Contacto();

               i.putExtra("CONTACTO",c);
               setResult(1,i);

            }
            else{
                String amigo="";
                if (checkAmigo.isChecked()){
                    amigo = this.getResources().getString(R.string.amigoSi);
                }
                else{
                    amigo = this.getResources().getString(R.string.amigoNo);
                }
                c = new Contacto(nombre.getText().toString(), sexo.getSelectedItem().toString(), correo.getText().toString(), telefono.getText().toString(), amigo);
                i.putExtra("CONTACTO",c);
                setResult(0, i);

            }
            finish();
        }

    }
}
