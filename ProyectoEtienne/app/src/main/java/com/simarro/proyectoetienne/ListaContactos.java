package com.simarro.proyectoetienne;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class ListaContactos extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton addContacto;
    private RecyclerView recyclerView;
    private AdaptadorContactos adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Contacto> contactos = new ArrayList<>();
    Contacto c;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_contactos);
        addContacto=findViewById(R.id.add_cotacto);
        addContacto.setOnClickListener(this);
        recyclerView=findViewById(R.id.r_View);
        recyclerView.setVisibility(View.INVISIBLE);
        adaptador =new AdaptadorContactos(this, contactos);
        adaptador.setOnClickListener(this);
        recyclerView.setAdapter(adaptador);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(layoutManager);
        crearGestos();
    }
    @Override
    protected void onResume() {
        super.onResume();
        this.adaptador.notifyDataSetChanged();
    }

    private void crearGestos() {
        ItemTouchHelper.SimpleCallback myCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                switch (direction) {
                    case (ItemTouchHelper.RIGHT): {
                        borrarContacto(viewHolder);
                        break;
                    }
                    case (ItemTouchHelper.LEFT): {
                        editarContacto(viewHolder.getAdapterPosition());
                        break;
                    }
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                // derecha
                if (dX > 0) {
                    moverHaciaLaDerecha(c, recyclerView, viewHolder, dX);
                } else if (dX < 0) { // izquierda
                    moverHaciaLaIzquierda(c, recyclerView, viewHolder, dX);
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            private void moverHaciaLaDerecha(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX) {
                Paint pincel = new Paint();
                pincel.setColor(Color.WHITE);
                int sizeText = getResources().getDimensionPixelSize(R.dimen.textSize);
                pincel.setTextSize(sizeText);

                c.clipRect(viewHolder.itemView.getLeft(), viewHolder.itemView.getTop(), dX, viewHolder.itemView.getBottom());
                if (dX < recyclerView.getWidth() / 3)
                    c.drawColor(getResources().getColor(R.color.borrarContacto));
                else
                    c.drawColor(getResources().getColor(R.color.borrarContacto));
                Drawable deleteThis = getDrawable(R.mipmap.ic_borrar);
                int margin = getResources().getDimensionPixelSize(R.dimen.margen);
                deleteThis.setBounds(viewHolder.itemView.getLeft() + margin,
                        viewHolder.itemView.getTop() + margin,
                        viewHolder.itemView.getHeight() - margin,
                        viewHolder.itemView.getBottom() - margin);
                deleteThis.draw(c);

                pincel.setTextAlign(Paint.Align.LEFT);
                c.drawText(getResources().getString(R.string.eliminar), viewHolder.itemView.getHeight(),
                        viewHolder.itemView.getBottom() - viewHolder.itemView.getHeight() / 2 + sizeText / 2, pincel);
            }

            private void moverHaciaLaIzquierda(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX) {
                Paint pincel = new Paint();
                pincel.setColor(Color.WHITE);
                int sizeText = getResources().getDimensionPixelSize(R.dimen.textSize);
                pincel.setTextSize(sizeText);

                c.clipRect(viewHolder.itemView.getRight() + dX, viewHolder.itemView.getTop(),
                        viewHolder.itemView.getRight(), viewHolder.itemView.getBottom());
                if (Math.abs(dX) < recyclerView.getWidth() / 3)
                    c.drawColor(getResources().getColor(R.color.editarContacto));
                else
                    c.drawColor(getResources().getColor(R.color.editarContacto));
                Drawable iconEdit = getDrawable(R.mipmap.ic_editar);
                int margin = getResources().getDimensionPixelSize(R.dimen.margen);
                iconEdit.setBounds(viewHolder.itemView.getRight() - viewHolder.itemView.getHeight() + margin,
                        viewHolder.itemView.getTop() + margin,
                        viewHolder.itemView.getRight() - margin,
                        viewHolder.itemView.getBottom() - margin);
                iconEdit.draw(c);

                pincel.setTextAlign(Paint.Align.RIGHT);
                c.drawText(getResources().getString(R.string.editar), viewHolder.itemView.getRight() - viewHolder.itemView.getHeight(),
                        viewHolder.itemView.getBottom() - viewHolder.itemView.getHeight() / 2 + sizeText / 2, pincel);
            }


        };
        (new ItemTouchHelper(myCallback)).attachToRecyclerView(recyclerView);
    }

    private void editarContacto(int adapterPosition) {
        Intent i=new Intent(ListaContactos.this,EditarContacto.class);
        Contacto c = this.contactos.get(adapterPosition);
        i.putExtra("pos",adapterPosition);
        i.putExtra("CONTACTO",c);
        startActivityForResult(i,2);
    }

    private void borrarContacto(@NonNull RecyclerView.ViewHolder viewHolder) {
        try {
            this.contactos.remove(viewHolder.getAdapterPosition());
        } catch (IndexOutOfBoundsException e) {

        }
        adaptador.notifyItemRemoved(viewHolder.getAdapterPosition());


    }

    @Override
    public void onClick(View v) {
        if (v.getId()==addContacto.getId()){
            Intent i = new Intent(ListaContactos.this, FormularioContacto.class);
            startActivityForResult(i, 0);
        }
      /*  int posicion = recyclerView.getChildAdapterPosition(v);
        Contacto cSeleccionado = contactos.get(posicion);
        Toast.makeText(this, "Contacto" + cSeleccionado.getNombre(), Toast.LENGTH_LONG).show();

       */
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode==0 && resultCode==0){
            c = (Contacto) data.getSerializableExtra("CONTACTO");
            this.contactos.add(0,c);
            recyclerView.setVisibility(View.VISIBLE);
            this.adaptador.notifyItemInserted(0);
            Snackbar snack = Snackbar.make(recyclerView, "Contacto Creado ",Snackbar.LENGTH_LONG);
            snack.show();
            snack.setAction("Deshacer", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contactos.remove(c);
                    adaptador.notifyDataSetChanged();
                }
            });

        }
        else if(requestCode==2 && resultCode==0){
            Contacto r = (Contacto)data.getSerializableExtra("CONTACTO");
            int pos= data.getIntExtra("pos", 0);
            this.contactos.set(pos,r);
            adaptador.notifyDataSetChanged();
        }
        else{
            Toast.makeText(this,"ERROR",Toast.LENGTH_LONG).show();
        }
    }
}
