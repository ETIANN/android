package com.simarro.proyectoetienne;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class ContactosViewHolder extends RecyclerView.ViewHolder {

    private ImageView foto;
    private TextView tvNombre;
    private TextView sexo;
    private TextView tvCorreo;
    private TextView telefono;
    private Context contexto;
    private TextView amigo;

    public ContactosViewHolder(View itemView, Context context) {
        super(itemView);

        foto = itemView.findViewById(R.id.img_contacto);
        tvNombre = itemView.findViewById(R.id.tv_nombre);
        sexo = itemView.findViewById(R.id.tv_sexo);
        tvCorreo = itemView.findViewById(R.id.tv_correo);
        telefono = itemView.findViewById(R.id.telefono);
        amigo= itemView.findViewById(R.id.tv_amigo);
        contexto = context;
    }

    public void bindAlumno(Contacto c) {

        tvNombre.setText(c.getNombre());
        sexo.setText(c.getSexo());
        tvCorreo.setText(c.getCorreo());
        telefono.setText(c.getTelefono());
        foto.setImageDrawable(contexto.getDrawable(R.drawable.cont));
        amigo.setText(c.getAmigo());

    }

}

