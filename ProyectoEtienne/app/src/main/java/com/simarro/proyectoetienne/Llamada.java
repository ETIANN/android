package com.simarro.proyectoetienne;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class Llamada extends AppCompatActivity implements View.OnClickListener {

    EditText numeroTel;
    ImageButton llamar;
    private final int TEL_COD = 100;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llamada);
        numeroTel=findViewById(R.id.editTextLlamada);
        llamar=findViewById(R.id.botonLlamada);
        llamar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        String phoneNumber = numeroTel.getText().toString();
        if (phoneNumber != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, TEL_COD);
            } else{
                OlderVersions(phoneNumber);

            }

        }
    }

    private void OlderVersions(String phoneNumber){
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));

        int result = checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE);
        if ( result == PackageManager.PERMISSION_GRANTED){

            startActivity(intentCall);
        }
        else{
            Toast.makeText(Llamada.this, "Acceso no autorizado", Toast.LENGTH_LONG).show();

        }
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case TEL_COD:
                String permisos = permissions[0];
                int result = grantResults[0];
                if (permisos.equals(Manifest.permission.CALL_PHONE)){
                    if (result == PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = numeroTel.getText().toString();
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED) return;
                        startActivity(intentCall);

                    }
                    else{
                        Toast.makeText(Llamada.this, "Acceso no autorizado", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    private boolean CheckPermission(String permission){
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
