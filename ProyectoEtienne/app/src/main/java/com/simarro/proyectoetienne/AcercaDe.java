package com.simarro.proyectoetienne;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AcercaDe extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton atras;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acerca_de);
        atras = findViewById(R.id.botonAtras);
        atras.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
