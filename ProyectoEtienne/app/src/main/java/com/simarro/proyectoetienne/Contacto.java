package com.simarro.proyectoetienne;

import java.io.Serializable;

public class Contacto implements Serializable {
    private String nombre, sexo, correo, telefono, foto, amigo;

    public Contacto() {

    }

    public Contacto(String nombre, String sexo, String correo, String telefono, String amigo) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.correo = correo;
        this.telefono = telefono;
        this.foto=foto;
        this.amigo=amigo;
    }

    public String getAmigo() {
        return amigo;
    }

    public void setAmigo(String amigo) {
        this.amigo = amigo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
