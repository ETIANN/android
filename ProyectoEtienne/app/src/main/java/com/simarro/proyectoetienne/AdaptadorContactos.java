package com.simarro.proyectoetienne;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdaptadorContactos extends RecyclerView.Adapter<ContactosViewHolder> implements View.OnClickListener {
    private ArrayList<Contacto> contactos;
    private Context context;
    private View.OnClickListener mListener;

    public AdaptadorContactos( Context context, ArrayList<Contacto> contactos) {
        this.contactos = contactos;
        this.context = context;
    }


    public ContactosViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacto, parent, false);
        ContactosViewHolder viewHolder = new ContactosViewHolder(itemView, this.context);

        itemView.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder( ContactosViewHolder viewholder, int position) {
        Contacto alumno= contactos.get(position);
        viewholder.bindAlumno(alumno);
    }

    @Override
    public int getItemCount() {
        return  this.contactos.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View view) {
        if (mListener!=null){
            mListener.onClick(view);
        }
    }
}
