package com.simarro.proyectoetienne;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class EditarContacto extends AppCompatActivity implements View.OnClickListener  {

    EditText nombre, correo, telefono;
    Spinner sexo;
    Button editar;
    CheckBox checkAmigo;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edita_contacto);
        nombre=findViewById(R.id.edtetf_nombre);
        correo=findViewById(R.id.edtetf_correo);
        telefono=findViewById(R.id.edtetf_telf);
        sexo=findViewById(R.id.edtspinner);
        editar=findViewById(R.id.bEditar);
        checkAmigo=findViewById(R.id.checkBox);
        editar.setOnClickListener(this);

        Intent i= getIntent();
        Contacto c=(Contacto)i.getSerializableExtra("CONTACTO");
        pos=i.getIntExtra("pos",0);
        nombre.setText(c.getNombre());
        correo.setText(c.getCorreo());
        telefono.setText(c.getTelefono());
        if(c.getSexo().equals("Hombre")){
            sexo.setSelection(0);
        }
        else{
            sexo.setSelection(1);
        }
        if (c.getAmigo().equals("Amigo")){
            checkAmigo.setChecked(true);
        }
        else if (c.getAmigo().equals("Desconocido")){
            checkAmigo.setChecked(false);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        String amigo="";
        if (checkAmigo.isChecked()){
            amigo = this.getResources().getString(R.string.amigoSi);
        }
        else{
            amigo = this.getResources().getString(R.string.amigoNo);
        }
        Contacto c= new Contacto(nombre.getText().toString(),sexo.getSelectedItem().toString(),correo.getText().toString(),telefono.getText().toString(),amigo);
        i.putExtra("CONTACTO", c);
        i.putExtra("pos",pos);
        setResult(0,i);
        finish();
    }
}
