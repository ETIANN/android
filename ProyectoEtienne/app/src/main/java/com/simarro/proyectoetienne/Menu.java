package com.simarro.proyectoetienne;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class Menu extends AppCompatActivity implements View.OnClickListener {

    TextView saludo;



    Button acerca, listaCont, llamada;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        saludo = findViewById(R.id.saludo);
        Bundle nomRecu = this.getIntent().getExtras();



        String nombre[]=nomRecu.getString("NOMBRE").split("@");

        saludo.setText(saludo.getText()+" "+nombre[0]);

        acerca=findViewById(R.id.bAcerca);
        acerca.setOnClickListener(this);
        listaCont=findViewById(R.id.bLista);
        listaCont.setOnClickListener(this);
        llamada=findViewById(R.id.bllamada);
        llamada.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()){
            case R.id.bAcerca:
                i = new Intent(Menu.this, AcercaDe.class);
                startActivity(i);
                break;
            case R.id.bLista:
               i = new Intent(Menu.this, ListaContactos.class);
               startActivity(i);
               break;
            case R.id.bllamada:
                i = new Intent(Menu.this,Llamada.class);
                startActivity(i);
                break;

        }
    }
}
