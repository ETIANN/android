package com.simarro.actividad_5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText email, password;
    Button entrar;
    Switch visiblePasss;
    TextView pistaPass, olvidaPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        entrar=findViewById(R.id.bEntrar);
        visiblePasss=findViewById(R.id.visiblePass);
        pistaPass=findViewById(R.id.pistaPass);
        olvidaPass=findViewById(R.id.olvidaPass);
        olvidaPass.setOnClickListener(this);
       email.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!email.getText().toString().contains("@")){
                    email.setError("Email no valido!!");
                }
           }

           @Override
           public void afterTextChanged(Editable s) {

           }
       });

       visiblePasss.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

               if (isChecked){
                   password.setInputType(InputType.TYPE_CLASS_TEXT);
               }
               else{
                  password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
               }
           }
       });


    }

    @Override
    public void onClick(View v) {
        pistaPass.setVisibility(View.VISIBLE);
    }
}
